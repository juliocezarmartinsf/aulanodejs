const readline = require ('readline-sync')

const a = parseFloat (readline.question('Valor de "A": '));
const b = parseFloat (readline.question('Valor de "B": '));
const c = parseFloat (readline.question('Valor de "C": '));


function teste(a, b, c){
    if(Math.abs(a-b) < c && c < (a+b)) 
    return true;

    else return false;
}

console.log("------------------------------------------");

if(a != 0 && b != 0 && c != 0){
    if(teste(a, b, c ) &&
        teste(a, c, b) &&
        teste(b, c, a)) console.log("é triangulo!");
    else console.log("não é triangulo!");
}else console.log("não é permitido valor nulo!");

console.log("");
